<?php
	/**
	* 
	*/
	class Database{
		private $_data = array();
		private static $_instance= null;
		private $_pdo,//connects to the database
				$_query,//returns the last query run
				$_error,		
				$_results,//returns the results
				$_count=0;//counts the number of results returned

		public function __construct() {//automatically instantiates the connection to the database
			try{
				$this->_pdo = new PDO('pgsql:host=localhost;port=5432; dbname=boda;' ,'postgres','postgres');
			
				
			} catch(PDOException $e) {
				die
				($e->getMessage());
			}
		}

	//this method checks whether there is an exsiting connection into the database and if non exists,it creates a new one
	public static function getinstance(){
		if(!isset(self::$_instance)){
			self::$_instance = new Database();
		}
		return self::$_instance;
	}
	
	public function query($sql, $params = array()){
		$this->_error = false;
		if($this->_query = $this->_pdo->prepare($sql)){
			if(count($params)){
				$x = 1;
				foreach ($params as $param) {
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}

			if($this->_query->execute()){
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
			}else{
				$this->_error = true;
			}
		}

		return $this;
	}

	public function action($action, $table, $where = array()){
		if(count($where) === 3){
			$operators = array('=', '>', '<', '>=', '<=');

			$field = $where[0];
			$operator = $where[1];
			$value = $where[2];

			if(in_array($operator, $operators)){
				$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

				if(!$this->query($sql, array($value))->error()){
					return $this;
				}
			}
		}
		return false;
	}

	// Helper functions
	public function get($table, $where){
		return $this->action('SELECT *', $table, $where);
	}

	public function delete($table, $where){
		return $this->action('DELETE', $table, $where);
	}

	public function insert($table, $fields = array()){
		if(count($fields)){
			$keys = array_keys($fields);
			$values = null;
			$x = 1;

			foreach ($fields as $field) {
				$values .= "?";

				if($x < count($fields)){
					$values .= ", ";
					$x++;
				}
			}

			$sql = "INSERT INTO {$table}(`".implode('`, `', $keys)."`) VALUES({$values})";
			if(!$this->query($sql, $fields)->error()){
				return true;
			}
		}	
	}

	public function update($table, $id, $fields){
		$set = '';
		$x = 1;

		foreach ($fields as $name => $value) {
			$set .= "{$name} = ?";
			if($x < count($fields)){
				$set .= ', ';
			}
			$x++;
		}

		$sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";
		if(!$this->query($sql, $fields)->error()){
			return true;
		}
	}

	public function results(){
		return $this->_results;
	}

	public function first(){
		// used for getting the first result in the result set
		return $this->results()[0];
	}

	public function error(){
		return $this->_error;
	}
}
?>